import {Factory, faker} from 'ember-cli-mirage';

export default Factory.extend({
  title() {
    return faker.lorem.words(faker.random.number({min: 2, max: 5}));
  },
  description(){
    return faker.lorem.paragraphs(4);
  },
  date(){
    return faker.date.recent(30*6);
  },
  imageUrl(){
    return faker.image.imageUrl(420, 225, 'city', true);
  }
});
