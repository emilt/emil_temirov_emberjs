import {Factory, faker} from 'ember-cli-mirage';

export default Factory.extend({
  title(){
    return faker.lorem.word();
  },

  color(){
    return faker.internet.color();
  }
});
