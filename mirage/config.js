export default function () {

  // These comments are here to help you get started. Feel free to delete them.

  /*
   Config (with defaults).

   Note: these only affect routes defined *after* them!
   */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = 'api';    // make this `api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
   Shorthand cheatsheet:

   this.get('/posts');
   this.post('/posts');
   this.get('/posts/:id');
   this.put('/posts/:id'); // or this.patch
   this.del('/posts/:id');

   http://www.ember-cli-mirage.com/docs/v0.2.x/shorthands/
   */

  this.get('/api/events', (schema, request) => {
    if (request.queryParams.searchQuery) {
      return schema.events.where((event) => {
        return event.title.toLowerCase().indexOf(request.queryParams.searchQuery.toLowerCase()) >= 0;
      });
    } else if (request.queryParams.category) {
      return schema.events.where((event) => {
        return event.categoryId === request.queryParams.category;
      });
    } else {
      return schema.events.all();
    }
  });

  this.get('/api/events/:id', ({events}, request) => {
    return events.find(request.params.id);
  });

  this.post('/api/events', (schema, request) => {
    var params = JSON.parse(request.requestBody);
    params.date = new Date();
    return schema.events.create(params);
  });

  this.get('/api/categories', (schema) => {
    return schema.categories.all();
  });

  this.passthrough('/locales/**');
}
