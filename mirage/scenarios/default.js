import {faker} from 'ember-cli-mirage';

export default function (server) {

  /*
   Seed your development database using your factories.
   This data will not be loaded in your tests.

   Make sure to define a factory for each model you want to create.
   */


  var cats = server.createList('category', 6);
  for(let i=0; i<65; i++){
    let cat = faker.list.random.apply(faker.list, cats)();
    server.create('event', {category: cat});
  }
}
