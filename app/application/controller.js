import Ember from 'ember';

export default Ember.Controller.extend({
  searchController: Ember.inject.controller('search'),
  searchQuery: Ember.computed.alias('searchController.searchQuery'),

  searchQueryObserver: Ember.observer('searchQuery', function () {
    this.transitionToRoute('search', {queryParams: {searchQuery: this.get('searchQuery')}});
  })
});
