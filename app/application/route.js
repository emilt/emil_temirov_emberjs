import Ember from 'ember';

export default Ember.Route.extend({
  i18n: Ember.inject.service(),
  beforeModel(){
    return this.get('i18n').initLibraryAsync();
  },

  model(){
    return this.get('store').findAll('category');
  }
});
