import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['category'],
  category: '',
  appController: Ember.inject.controller('application'),
  categories: Ember.computed.oneWay('appController.model'),

  showEventForm: false,

  actions: {
    onFilter(category){
      if (category == null) {
        this.transitionToRoute({queryParams: {category: ''}});
      } else {
        this.transitionToRoute({queryParams: {category: category.get('id')}});
      }
    },
    openEventForm(){
      this.set('showEventForm', true);
    },
    closeEventForm(state){
      var me = this;
      switch (state) {
        case 'cancel':
          me.set('showEventForm', false);
          break;
        case 'ok':
          var newEvent = me.get('store').createRecord('event', {
            title: me.get('newEventTitle'),
            description: me.get('newEventDescription'),
            imageUrl: me.get('newEventImageUrl'),
            category: me.get('newEventCategory')
          });
          newEvent.save().then((event) => {
            me.set('newEventTitle', '');
            me.set('newEventDescription', '');
            me.set('newEventImageUrl', '');
            me.transitionToRoute('event', event);
          });
          break;
      }
    },
    goToDetails(event){
      this.transitionToRoute('event', event);
    }
  },

  currentCategory: Ember.computed('category', function () {
    var catId = this.get('category');
    if (catId) {
      return this.get('store').peekRecord('category', parseInt(catId, 10));
    }
  }),

  categoriesObserver: Ember.on('init', Ember.observer('categories', function () {
    if (this.get('categories')) {
      this.set('newEventCategory', this.get('categories').objectAt(0));
    }
  }))
});
