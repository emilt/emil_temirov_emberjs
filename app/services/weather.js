import Ember from 'ember';
import ENV from '../config/environment';

/* global geoLite */

export default Ember.Service.extend(Ember.$.weatherWidget, {
  defaultConfig: {
    celsius: true,
    key: ENV.forecastApiKey
  },

  makeURL(options){
    let url = options.url + options.key + '/' + options.lat + ',' + options.lon;
    if (options.date) {
      url += ',' + options.date;
    }
    return url;
  },

  detectGeoLocation(){
    return new Ember.RSVP.Promise(function(resolve, reject) {
      (new geoLite()).locate(resolve, reject);
    });
  },

  pollAPI(options, callback) {
    options = Ember.$.extend(this.get('defaultConfig'), options);
    return Ember.$.weatherWidget.pollAPI(options, callback);
  }
});
