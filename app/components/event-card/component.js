import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['event-card'],
  actions: {
    goToDetails(event){
      this.sendAction('onOpenDetail', event);
    }
  }
});
