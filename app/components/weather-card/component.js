import Ember from 'ember';

export default Ember.Component.extend({
  weather: Ember.inject.service(),
  data: null,
  date: null,

  ICONS: {
    'clear-day': 'day-sunny',
    'clear-night': 'night-clear',
    'rain': 'rain',
    'snow': 'snow',
    'sleet': 'sleet',
    'wind': 'windy',
    'fog': 'fog',
    'cloudy': 'cloudy',
    'partly-cloudy-day': 'day-cloudy',
    'partly-cloudy-night': 'night-cloudy'
  },

  didInsertElement(){
    var me = this;
    me._super(...arguments);
    var date = me.get('date');

    if (!date){
      date = new Date();
    }

    me.get('weather').detectGeoLocation().then(coords => {
      me.get('weather').pollAPI({
        lat: coords.latitude,
        lon: coords.longitude,
        date: `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`
      }, res=> {
        me.set('data', res.currently);
        console.log(res.currently);
      });
    });
  },

  icon: Ember.computed('data', function(){
    if (this.get('data')) {
      return this.get('ICONS')[this.get('data').icon];
    } else {
      return 'N/A';
    }
  }),

  celsius: Ember.computed('data', function(){
    if (this.get('data')) {
      return Math.round(this.get('data').temperature - 32 / 1.8);
    } else {
      return 'N/A';
    }
  })
});
