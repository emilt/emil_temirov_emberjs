import Ember from 'ember';
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import {belongsTo} from 'ember-data/relationships';

export default Model.extend({
  title: attr('string'),
  description: attr('string'),
  date: attr('date'),
  imageUrl: attr('string'),
  category: belongsTo('category'),

  dateString: Ember.computed('date', function () {
    var d = this.get('date');
    if (d){
      return `${d.getFullYear()}-${d.getMonth()}-${d.getDate()}`;
    }
  }),

  shortDescription: Ember.computed('description', function () {
    return (this.get('description') || '').slice(0, 200);
  })
});
