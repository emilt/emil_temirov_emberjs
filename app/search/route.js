import Ember from 'ember';

export default Ember.Route.extend({
  queryParams: {
    searchQuery: {
      refreshModel: true
    }
  },

  model(params){
    if (!params.searchQuery) {
      return this.transitionTo('events');
    }
    return this.get('store').query('event', params);
  }
});
