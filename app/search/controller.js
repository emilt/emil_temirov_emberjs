import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['searchQuery'],
  searchQuery: '',
  actions: {
    goToDetails(event){
      this.transitionToRoute('event', event);
    }
  },
  groupedEvents: Ember.computed('model.@each.date', function () {
    var items = this.get('model');
    var groups = new Ember.A();
    if (items) {
      items.forEach(function (item) {
        var value = item.get('dateString');
        var group = groups.findBy('dateString', value);

        if (Ember.isPresent(group)) {
          group.get('events').push(item);
        } else {
          group = Ember.Object.create({dateString: value, events: [item]});
          groups.push(group);
        }
      });
    }
    return groups;
  })
});
